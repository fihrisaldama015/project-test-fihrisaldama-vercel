"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

function NavbarMenu() {
  const pathname = usePathname();

  return (
    <ul className="flex gap-7 text-white font-medium tracking-tight text-lg [&>*]:py-2 [&>*]:cursor-pointer">
      <Link href={"/work"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/work" ? "border-slate-100" : "border-transparent"
          }`}
        >
          Work
        </li>
      </Link>
      <Link href={"/about"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/about" ? "border-slate-100" : "border-transparent"
          }`}
        >
          About
        </li>
      </Link>
      <Link href={"/services"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/services" ? "border-slate-100" : "border-transparent"
          }`}
        >
          Services
        </li>
      </Link>
      <Link href={"/"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/" ? "border-slate-100" : "border-transparent"
          }`}
        >
          Ideas
        </li>
      </Link>
      <Link href={"/careers"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/careers" ? "border-slate-100" : "border-transparent"
          }`}
        >
          Careers
        </li>
      </Link>
      <Link href={"/contact"}>
        <li
          className={`border-b-[5px] hover:border-slate-100 transition-all duration-150 ${
            pathname === "/contact" ? "border-slate-100" : "border-transparent"
          }`}
        >
          Contact
        </li>
      </Link>
    </ul>
  );
}

export default NavbarMenu;

import Image from "next/image";

function Banner() {
  return (
    <div className="relative h-[400px]">
      <Image
        src="/img/banner.jpg"
        alt="Suitmedia Banner"
        fill
        className="object-cover filter brightness-50"
        style={{ clipPath: "polygon(0 0%, 100% 0, 100% 70%, 0 100%)" }}
      />
      <div className="absolute top-[40%] left-1/2 transform -translate-x-1/2 -translate-y-1/2 text-center">
        <h1 className="text-white text-5xl font-normal">Ideas</h1>
        <p className="text-white text-lg font-medium">
          Where all our great things begin
        </p>
      </div>
    </div>
  );
}

export default Banner;

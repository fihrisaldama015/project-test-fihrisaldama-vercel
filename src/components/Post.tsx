"use client";
import Image from "next/image";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useCallback } from "react";

type Link = {
  url: string;
  label: string;
  active: boolean;
};

type PostMetaProps = {
  current_page: number;
  from: number;
  to: number;
  total: number;
  last_page: number;
  links: Link[];
  path: string;
  per_page: number;
};

function Post({ meta, post }: { meta: PostMetaProps; post: Post[] }) {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const createQueryString = useCallback(
    (name: string, value: string) => {
      const params = new URLSearchParams(searchParams);
      params.set(name, value);
      params.set("append[]", "medium_image");

      return params.toString();
    },
    [searchParams]
  );

  const handleSort = (sortValue: Sort) => {
    router.push(pathname + "?" + createQueryString("sort", `${sortValue}`));
  };

  const handlePostsPerPage = (postsPerPageValue: number) => {
    const url =
      pathname + "?" + createQueryString("page[size]", `${postsPerPageValue}`);

    if (meta.current_page * postsPerPageValue > meta.total) {
      const params = new URLSearchParams(
        createQueryString("page[size]", `${postsPerPageValue}`)
      );
      params.set(
        "page[number]",
        Math.floor(meta.total / postsPerPageValue).toString()
      );
      router.push(pathname + "?" + params.toString());
      return;
    }
    router.push(
      pathname + "?" + createQueryString("page[size]", `${postsPerPageValue}`)
    );
  };

  const handlePageRedirect = (url: string | null) => {
    if (!url) return;

    const params = new URLSearchParams(url.split("?")[1]);
    if (!params.has("page[number]")) return;

    if (params.get("page[number]") === String(meta.current_page)) return;

    const page = params.get("page[number]");

    router.push(pathname + "?" + createQueryString("page[number]", `${page}`));
  };

  const generateDate = (published_at: string) => {
    const formattedDate = new Date(published_at).toLocaleDateString("id-ID", {
      year: "numeric",
      month: "long",
      day: "numeric",
    });

    return formattedDate.toUpperCase();
  };

  return (
    <div className="py-8 px-28 min-h-[100vh] flex flex-col items-center justify-start bg-white">
      <div className="w-full text-sm font-medium text-slate-800 flex flex-row justify-between">
        <p>
          Showing {meta?.from} to {meta?.to} of {meta?.total}
        </p>
        <form action="" className="flex gap-3 items-center">
          <p>Show per page:</p>
          <select
            name="show"
            id="show"
            onChange={(e) => handlePostsPerPage(Number(e.target.value))}
            defaultValue={meta?.per_page}
            className="border border-gray-300 bg-white rounded-full px-4 py-2 focus:outline-orange-400/50"
          >
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
          </select>
          <p className="ml-6">Sort by:</p>
          <select
            name="show"
            id="show"
            onChange={(e) => handleSort(e.target.value as Sort)}
            defaultValue={"-published_at"}
            className="border border-gray-300 bg-white rounded-full px-4 py-2 focus:outline-orange-400/50"
          >
            <option value="-published_at">Newest</option>
            <option value="published_at">Oldest</option>
          </select>
        </form>
      </div>
      <div className="my-12 grid xl:grid-cols-4 md:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-9">
        {post?.map((post: Post, id: number) => (
          <div
            key={id}
            className="flex flex-col gap-4 items-center ring-1 ring-slate-400/10 shadow-2xl shadow-slate-600/15 rounded-lg"
          >
            <div className="w-full">
              <Image
                src={
                  post?.medium_image && post?.medium_image.length > 0
                    ? post?.medium_image[0]?.url
                    : "/img/no_image.png"
                }
                alt={post?.title}
                width={800}
                height={800}
                className="w-full h-48 shadow-sm object-cover rounded-tl-lg rounded-tr-lg"
              />
            </div>
            <div className="p-6">
              <span className="text-slate-400 tracking-tighter text-xs font-medium">
                {generateDate(post?.published_at)}
              </span>
              <p
                className="text-lg tracking-tighter font-medium text-slate-900"
                style={{ lineHeight: "1.2em" }}
              >
                {post?.title}
              </p>
            </div>
          </div>
        ))}
      </div>
      <div>
        {meta?.links?.map((link: Link, id: number) => (
          <button
            key={id}
            onClick={() => handlePageRedirect(link.url)}
            className={`px-4 py-2 rounded-md text-sm ${
              link.active
                ? "bg-[#FA6834] text-white"
                : "bg-white text-slate-800"
            }
            ${link.url === null ? "opacity-50 cursor-not-allowed" : ""}`}
          >
            <p
              dangerouslySetInnerHTML={{ __html: link.label }}
              style={{ fontWeight: "600" }}
            ></p>
          </button>
        ))}
      </div>
    </div>
  );
}

export default Post;

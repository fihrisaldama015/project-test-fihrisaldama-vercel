import Banner from "@/components/Banner";
import Post from "@/components/Post";
import { getIdeasMeta } from "@/utils/ideas";
import { createQueryString } from "@/utils/queryHelper";

export default async function Home({
  searchParams,
}: {
  searchParams: { [key: string]: string | undefined };
}) {
  const params = new URLSearchParams();
  params.set("page[size]", "10");
  params.set("sort", "-published_at");
  params.set("append[]", "medium_image");

  const query = createQueryString(searchParams, params);
  const { meta, data } = await getIdeasMeta(query);

  return (
    <main className="bg-slate-50">
      <Banner />
      <Post meta={meta} post={data} />
    </main>
  );
}
